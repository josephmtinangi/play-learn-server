package com.josephmtinangi.playlearnserver.specifications;

import com.josephmtinangi.playlearnserver.criteria.SearchCriteria;
import com.josephmtinangi.playlearnserver.models.Quiz;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import java.util.ArrayList;
import java.util.List;

public class QuizSpecificationsBuilder {

    private final List<SearchCriteria> params;

    public QuizSpecificationsBuilder() {
        this.params = new ArrayList<SearchCriteria>();
    }

    public QuizSpecificationsBuilder with(String key, String operation, Object value) {
        params.add(new SearchCriteria(key, operation, value));
        return this;
    }

    public Specification<Quiz> build() {
        if (params.size() == 0) {
            return null;
        }

        List<Specification<Quiz>> specs = new ArrayList<Specification<Quiz>>();
        for (SearchCriteria param : params) {
            specs.add(new QuizSpecification(param));
        }

        Specification<Quiz> result = specs.get(0);
        for (int i = 1; i < specs.size(); i++) {
            result = Specifications.where(result).and(specs.get(i));
        }
        return result;
    }
}
