package com.josephmtinangi.playlearnserver.services;


import com.josephmtinangi.playlearnserver.models.EmailOut;
import freemarker.core.ParseException;
import freemarker.template.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private Configuration freemarkerConfig;

    @Value("${EMAL_FROM_NAME}")
    private String fromName;

    public void sendSimpleEmail(EmailOut emailOut, String from) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        simpleMailMessage.setTo(emailOut.getRecipient());
        simpleMailMessage.setSubject(emailOut.getSubject());
        simpleMailMessage.setText(emailOut.getBody());
        simpleMailMessage.setFrom(from);

        javaMailSender.send(simpleMailMessage);
    }

    public void sendHtmlEmail(EmailOut emailOut, String from) throws MessagingException, TemplateNotFoundException, MalformedTemplateNameException, ParseException, IOException, TemplateException {

        MimeMessage mail = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mail, true);

        freemarkerConfig.setClassForTemplateLoading(this.getClass(), "/templates");

        Template template = freemarkerConfig.getTemplate("emails/app.ftl");

        Map<String, Object> model = new HashMap<String, Object>();
        model.put("subject", emailOut.getSubject());
        model.put("name", emailOut.getRecipientName());
        model.put("content", emailOut.getBody());

        String text = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);

        helper.setTo(emailOut.getRecipient().trim());
        helper.setSubject(emailOut.getSubject().trim());
        helper.setText(text, true);
        helper.setFrom(from, fromName.trim());

        javaMailSender.send(mail);

    }
}
