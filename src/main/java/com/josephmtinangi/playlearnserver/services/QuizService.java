package com.josephmtinangi.playlearnserver.services;

import com.josephmtinangi.playlearnserver.models.Quiz;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@Service
public class QuizService {

    @Autowired
    private EntityManager em;

    public List<Quiz> findRandomQuizzes(Long levelId, Long subjectId, Long difficutyId, Long categoryId, int questions) {

        String sql = "SELECT q FROM Quiz q WHERE q.level.id=" + levelId + " AND q.subject.id=" + subjectId + " AND q.difficulty.id=" + difficutyId + " AND q.category.id=" + categoryId + " ORDER BY random()";

        TypedQuery<Quiz> query = em.createQuery(sql, Quiz.class);
        query.setMaxResults(questions);

        return query.getResultList();
    }
}
