package com.josephmtinangi.playlearnserver.repositories;

import com.josephmtinangi.playlearnserver.models.Achievement;
import com.josephmtinangi.playlearnserver.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AchievementRepository extends JpaRepository<Achievement, Long> {

    Achievement findFirstByBattleIdAndUserId(Long battleId, Long userId);

    @Query("SELECT a FROM Achievement a WHERE a.battle.id=:id ORDER BY a.points DESC")
    List<Achievement> findAllByBattleId(@Param("id") Long id);

    @Query("SELECT SUM(a.points) FROM Achievement a WHERE a.user.id=:id")
    Integer findPointsByUserId(@Param("id") Long id);
}
