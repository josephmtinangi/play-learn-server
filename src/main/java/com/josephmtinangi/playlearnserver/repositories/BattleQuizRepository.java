package com.josephmtinangi.playlearnserver.repositories;

import com.josephmtinangi.playlearnserver.models.BattleQuiz;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BattleQuizRepository extends JpaRepository<BattleQuiz, Long> {
}
