package com.josephmtinangi.playlearnserver.repositories;

import com.josephmtinangi.playlearnserver.models.Attempt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttemptRepository extends JpaRepository<Attempt, Long> {

    @Query("select new map(q.id as quizId, q.question as question, q.optionOne as optionOne, q.optionTwo as optionTwo, q.optionThree as optionThree,q.optionFour as optionFour, q.correct as correct, a.response as response) from Attempt a, Battle b, Quiz q where a.battle.id=b.id and a.quiz.id=q.id and b.id=:battleId and a.user.id=:userId order by a.createdAt asc")
    List<Object> findAllByBattleIdAndUserId(@Param("battleId") Long id, @Param("userId") Long id1);
}
