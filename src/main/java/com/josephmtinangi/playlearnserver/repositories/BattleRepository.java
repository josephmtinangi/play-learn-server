package com.josephmtinangi.playlearnserver.repositories;

import com.josephmtinangi.playlearnserver.models.Battle;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BattleRepository extends PagingAndSortingRepository<Battle, Long> {
}
