package com.josephmtinangi.playlearnserver.repositories;

import com.josephmtinangi.playlearnserver.models.UserAuthentication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAuthenticationRepository extends JpaRepository<UserAuthentication, Long> {
}
