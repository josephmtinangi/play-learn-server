package com.josephmtinangi.playlearnserver.repositories;

import com.josephmtinangi.playlearnserver.models.Battle;
import com.josephmtinangi.playlearnserver.models.Participant;
import com.josephmtinangi.playlearnserver.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ParticipantRepository extends JpaRepository<Participant, Long> {

    @Query("SELECT DISTINCT p.user FROM Participant p WHERE p.battle.id=:id")
    List<User> findAllByBattleId(@Param("id") Long id);

    List<Participant> findAllByBattle(Battle battle);
}
