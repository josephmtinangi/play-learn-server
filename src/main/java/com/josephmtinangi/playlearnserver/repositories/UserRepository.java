package com.josephmtinangi.playlearnserver.repositories;

import com.josephmtinangi.playlearnserver.models.User;
import com.josephmtinangi.playlearnserver.utilities.Helper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.HashMap;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, Long> {

    User findFirstByUsername(String username);

    User findFirstByEmail(String email);
}
