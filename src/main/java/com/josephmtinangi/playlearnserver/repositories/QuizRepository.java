package com.josephmtinangi.playlearnserver.repositories;

import com.josephmtinangi.playlearnserver.models.Quiz;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QuizRepository extends PagingAndSortingRepository<Quiz, Long>, JpaSpecificationExecutor<Quiz> {
}
