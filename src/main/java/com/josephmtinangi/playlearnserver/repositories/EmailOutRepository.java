package com.josephmtinangi.playlearnserver.repositories;

import com.josephmtinangi.playlearnserver.models.EmailOut;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmailOutRepository extends JpaRepository<EmailOut, Long> {

    @Query("SELECT e FROM EmailOut e WHERE e.sentStatus = false and recipient != ''")
    List<EmailOut> findUnsentEmails();
}
