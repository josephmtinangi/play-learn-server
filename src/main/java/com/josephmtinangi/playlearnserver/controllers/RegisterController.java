package com.josephmtinangi.playlearnserver.controllers;

import com.josephmtinangi.playlearnserver.models.EmailOut;
import com.josephmtinangi.playlearnserver.models.User;
import com.josephmtinangi.playlearnserver.repositories.EmailOutRepository;
import com.josephmtinangi.playlearnserver.repositories.UserRepository;
import com.josephmtinangi.playlearnserver.utilities.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.UUID;

@Controller
@RequestMapping(path = "/register")
public class RegisterController {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private EmailOutRepository emailOutRepository;

	@Value("${APP_URL}")
	private String appUrl;

	@RequestMapping(path = "", method = RequestMethod.POST)
	public ResponseEntity<?> store(@ModelAttribute User user) {

		User existingUserWithTheSameEmail = userRepository.findFirstByEmail(user.getEmail());
		if (existingUserWithTheSameEmail != null) {
			return Helper.createResponse(Helper.createMessage("Email already exists"), HttpStatus.OK);
		}
		User existingUserWithTheSameUsername = userRepository.findFirstByUsername(user.getUsername());
		if (existingUserWithTheSameUsername != null) {
			return Helper.createResponse(Helper.createMessage("Username is already taken"), HttpStatus.OK);
		}

		BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		user.setToken(UUID.randomUUID().toString());
		user.setPoints(new Long(0));
		User newUser = userRepository.save(user);

		String body = "Confirm email: " + appUrl + "/email/" + user.getToken();

		// Send email
		EmailOut emailOut = new EmailOut();
		emailOut.setSubject("Confirm Email");
		emailOut.setRecipient(newUser.getEmail());
		emailOut.setRecipientName(newUser.getUsername());
		emailOut.setBody(body);

		emailOutRepository.save(emailOut);

		return Helper.createResponse(newUser, HttpStatus.CREATED);
	}
}
