package com.josephmtinangi.playlearnserver.controllers;

import com.josephmtinangi.playlearnserver.models.Achievement;
import com.josephmtinangi.playlearnserver.models.Battle;
import com.josephmtinangi.playlearnserver.models.User;
import com.josephmtinangi.playlearnserver.repositories.AchievementRepository;
import com.josephmtinangi.playlearnserver.repositories.BattleRepository;
import com.josephmtinangi.playlearnserver.repositories.UserRepository;
import com.josephmtinangi.playlearnserver.utilities.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/achievements")
public class AchievementsController {

    @Autowired
    private AchievementRepository achievementRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BattleRepository battleRepository;


    @Value("${POINTS_PER_QUESTION}")
    private Long pointsPerQuestion;

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<?> store(@ModelAttribute Achievement achievement) {

        User user = userRepository.findOne(Helper.getAuthenticatedUserId());
        Battle battle = battleRepository.findOne(achievement.getBattle().getId());

        Achievement oldAchievement = achievementRepository.findFirstByBattleIdAndUserId(battle.getId(), user.getId());

        achievement.setUser(user);

        Achievement newAchievement = null;
        if (oldAchievement != null) {
            // update
            achievement.setId(oldAchievement.getId());
            achievement.setPoints(oldAchievement.getPoints() + pointsPerQuestion);
        }
        user.setPoints(user.getPoints() + pointsPerQuestion);

        newAchievement = achievementRepository.save(achievement);

        return Helper.createResponse(newAchievement, HttpStatus.CREATED);
    }
}
