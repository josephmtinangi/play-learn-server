package com.josephmtinangi.playlearnserver.controllers;

import com.josephmtinangi.playlearnserver.models.Battle;
import com.josephmtinangi.playlearnserver.models.Participant;
import com.josephmtinangi.playlearnserver.models.User;
import com.josephmtinangi.playlearnserver.repositories.BattleRepository;
import com.josephmtinangi.playlearnserver.repositories.ParticipantRepository;
import com.josephmtinangi.playlearnserver.repositories.UserRepository;
import com.josephmtinangi.playlearnserver.utilities.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/participants")
public class ParticipantController {

    @Autowired
    private BattleRepository battleRepository;

    @Autowired
    private ParticipantRepository participantRepository;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(path = "/battles/{battleId}", method = RequestMethod.GET)
    public ResponseEntity<?> show(@PathVariable Long battleId) {

        Battle battle = battleRepository.findOne(battleId);

        if (battle == null) {
            return Helper.createResponse(null, HttpStatus.BAD_REQUEST);
        }

        List<Participant> participants = participantRepository.findAllByBattle(battle);

        return Helper.createResponse(participants, HttpStatus.OK);
    }

    @RequestMapping(path = "/battles/{battleId}/users/{userId}", method = RequestMethod.POST)
    public ResponseEntity<?> store(@PathVariable Long battleId, @PathVariable Long userId) {

        Battle battle = battleRepository.findOne(battleId);

        if (battle == null) {
            return Helper.createResponse(null, HttpStatus.BAD_REQUEST);
        }

        User user = userRepository.findOne(userId);

        if (user == null) {
            return Helper.createResponse(null, HttpStatus.BAD_REQUEST);
        }

        Participant participant = new Participant();
        participant.setBattle(battle);
        participant.setUser(user);
        participantRepository.save(participant);

        return Helper.createResponse(participant, HttpStatus.CREATED);
    }
}
