package com.josephmtinangi.playlearnserver.controllers;

import com.josephmtinangi.playlearnserver.repositories.*;
import com.josephmtinangi.playlearnserver.utilities.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping(path = "/stats")
public class StatsController {

    @Autowired
    private LevelRepository levelRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private DifficultyRepository difficultyRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BattleRepository battleRepository;

    @Autowired
    private AttemptRepository attemptRepository;

    @RequestMapping(path = "/dashboard", method = RequestMethod.GET)
    public ResponseEntity<?> dashboard() {

        HashMap<String, Object> output = new HashMap<>();
        output.put("levels", levelRepository.count());
        output.put("subjects", subjectRepository.count());
        output.put("difficulties", difficultyRepository.count());
        output.put("categories", categoryRepository.count());
        output.put("quizzes", quizRepository.count());
        output.put("users", userRepository.count());
        output.put("battles", battleRepository.count());
        output.put("attempts", attemptRepository.count());

        return Helper.createResponse(output, HttpStatus.OK);
    }
}
