package com.josephmtinangi.playlearnserver.controllers;


import com.josephmtinangi.playlearnserver.models.User;
import com.josephmtinangi.playlearnserver.repositories.AchievementRepository;
import com.josephmtinangi.playlearnserver.repositories.UserRepository;
import com.josephmtinangi.playlearnserver.utilities.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.ConstraintViolationException;
import java.util.HashMap;


@Controller
@RequestMapping(path = "/u")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @Value("${ROOT_STORAGE}")
    private String rootStorage;

    @RequestMapping(path = "/all", method = RequestMethod.GET)
    public ResponseEntity<?> index(Pageable pageable) {

        Page<User> users = userRepository.findAll(pageable);

        return Helper.createResponse(users, HttpStatus.OK);
    }

    @RequestMapping(path = "/{username}", method = RequestMethod.GET)
    public ResponseEntity<?> show(@PathVariable String username) {

        User user = userRepository.findFirstByUsername(username);

        if (user == null) {
            Helper.createResponse(Helper.createMessage("User not found"), HttpStatus.NOT_FOUND);
        }

        HashMap<String, Object> data = new HashMap<>();
        data.put("user", user);
        data.put("createdBattles", user.getBattles().size());
        data.put("participatedBattles", user.getParticipatedBattles().size());

        return Helper.createResponse(data, HttpStatus.OK);
    }

    @RequestMapping(path = "/{username}/own-battles", method = RequestMethod.GET)
    public ResponseEntity<?> ownBattles(@PathVariable String username) {

        User user = userRepository.findFirstByUsername(username);

        if (user == null) {
            Helper.createResponse(Helper.createMessage("User not found"), HttpStatus.NOT_FOUND);
        }

        return Helper.createResponse(user.getBattles(), HttpStatus.OK);
    }

    @RequestMapping(path = "/{username}/achievements")
    public ResponseEntity<?> achievements(@PathVariable String username) {

        User user = userRepository.findFirstByUsername(username);

        if (user == null) {
            Helper.createResponse(Helper.createMessage("User not found"), HttpStatus.NOT_FOUND);
        }

        return Helper.createResponse(user.getAchievements(), HttpStatus.OK);
    }

    @RequestMapping(path = "/{username}/avatar", method = RequestMethod.POST)
    public ResponseEntity<?> storeAvatar(@PathVariable String username, @RequestParam MultipartFile file) {

        User user = userRepository.findFirstByUsername(username);

        if (user == null) {
            return Helper.createResponse(Helper.createMessage("User not found"), HttpStatus.NOT_FOUND);
        }

        try {
            user.setAvatarUrl(Helper.saveFile(file, "avatars", rootStorage));
        } catch (Exception e) {
            e.printStackTrace();
            return Helper.createResponse(Helper.createMessage("Upload failed. " + e.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
        }
        User newUser = userRepository.save(user);

        return Helper.createResponse(newUser, HttpStatus.OK);
    }

    @RequestMapping(path = "/{userId}", method = RequestMethod.POST)
    public ResponseEntity<?> update(@PathVariable Long userId, @ModelAttribute User user) {

        User oldUser = userRepository.findOne(userId);

        if (oldUser == null) {
            return Helper.createResponse(Helper.createMessage("User not found"), HttpStatus.NOT_FOUND);
        }

        user.setId(oldUser.getId());
        user.setPassword(oldUser.getPassword());
        try {
            userRepository.save(user);
        } catch (Exception e) {
            return Helper.createResponse(Helper.createMessage(e.getMessage()), HttpStatus.BAD_REQUEST);
        }

        return Helper.createResponse(user, HttpStatus.OK);
    }
}
