package com.josephmtinangi.playlearnserver.controllers;

import com.josephmtinangi.playlearnserver.models.Difficulty;
import com.josephmtinangi.playlearnserver.repositories.DifficultyRepository;
import com.josephmtinangi.playlearnserver.utilities.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/difficulties")
public class DifficultiesController {

    @Autowired
    private DifficultyRepository difficultyRepository;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<?> index() {

        return Helper.createResponse(difficultyRepository.findAll(), HttpStatus.OK);
    }


    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<?> store(@ModelAttribute Difficulty difficulty) {

        return Helper.createResponse(difficultyRepository.save(difficulty), HttpStatus.OK);
    }
}
