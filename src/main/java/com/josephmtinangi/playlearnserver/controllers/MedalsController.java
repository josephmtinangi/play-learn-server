package com.josephmtinangi.playlearnserver.controllers;

import com.josephmtinangi.playlearnserver.models.Medal;
import com.josephmtinangi.playlearnserver.repositories.MedalRepository;
import com.josephmtinangi.playlearnserver.utilities.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/medals")
public class MedalsController {

    @Autowired
    private MedalRepository medalRepository;

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<?> store(@ModelAttribute Medal medal) {

        Medal newMedal = medalRepository.save(medal);

        return Helper.createResponse(newMedal, HttpStatus.CREATED);
    }
}
