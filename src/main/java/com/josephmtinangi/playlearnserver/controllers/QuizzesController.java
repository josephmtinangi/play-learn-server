package com.josephmtinangi.playlearnserver.controllers;

import com.josephmtinangi.playlearnserver.models.*;
import com.josephmtinangi.playlearnserver.repositories.*;
import com.josephmtinangi.playlearnserver.specifications.QuizSpecificationsBuilder;
import com.josephmtinangi.playlearnserver.utilities.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(path = "/quizzes")
public class QuizzesController {

    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private LevelRepository levelRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private DifficultyRepository difficultyRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<?> index(Pageable pageable, @RequestParam HashMap<String, String> params) {

        List<Object> paramsList = constructParams(params);


        QuizSpecificationsBuilder builder = new QuizSpecificationsBuilder();
        for (Object param : paramsList) {
            builder.with(param.getClass().getSimpleName().toLowerCase(), ":", param);
        }
        Specification<Quiz> specifications = builder.build();

        Page<Quiz> quizzes = quizRepository.findAll(specifications, pageable);

        return Helper.createResponse(quizzes, HttpStatus.OK);
    }

    private List<Object> constructParams(@RequestParam HashMap<String, String> params) {

        List<Object> paramsList = new ArrayList<>();

        if (params.get("level") != null && !params.get("level").trim().isEmpty()) {
            Level level = levelRepository.findOne(new Long(params.get("level").trim()));
            if (level != null) {
                paramsList.add(level);
            }
        }

        if (params.get("subject") != null && !params.get("subject").trim().isEmpty()) {
            Subject subject = subjectRepository.findOne(new Long(params.get("subject").trim()));
            if (subject != null) {
                paramsList.add(subject);
            }
        }

        if (params.get("difficulty") != null && !params.get("difficulty").trim().isEmpty()) {
            Difficulty difficulty = difficultyRepository.findOne(new Long(params.get("difficulty").trim()));
            if (difficulty != null) {
                paramsList.add(difficulty);
            }
        }

        if (params.get("category") != null && !params.get("category").trim().isEmpty()) {
            Category category = categoryRepository.findOne(new Long(params.get("category").trim()));
            if (category != null) {
                paramsList.add(category);
            }
        }

        if (params.get("user") != null && !params.get("user").trim().isEmpty()) {
            User user = userRepository.findOne(new Long(params.get("user").trim()));
            if (user != null) {
                paramsList.add(user);
            }
        }

        return paramsList;
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<?> store(@ModelAttribute Quiz quiz) {

        quiz.setUser(userRepository.findOne(Helper.getAuthenticatedUserId()));

        Quiz newQuiz = quizRepository.save(quiz);

        return Helper.createResponse(newQuiz, HttpStatus.CREATED);
    }

    @RequestMapping(path = "/upload", method = RequestMethod.POST)
    public ResponseEntity<?> upload(@RequestParam Long levelId, @RequestParam Long subjectId, @RequestParam Long difficultyId, @RequestParam Long categoryId, @RequestParam MultipartFile file) {

        Level level = levelRepository.findOne(levelId);
        if (level == null) {
            return Helper.createResponse(Helper.createMessage("Level not found"), HttpStatus.BAD_REQUEST);
        }
        Subject subject = subjectRepository.findOne(subjectId);
        if (subject == null) {
            return Helper.createResponse(Helper.createMessage("subject not found"), HttpStatus.BAD_REQUEST);
        }
        Difficulty difficulty = difficultyRepository.findOne(difficultyId);
        if (difficulty == null) {
            return Helper.createResponse(Helper.createMessage("difficulty not found"), HttpStatus.BAD_REQUEST);
        }
        Category category = categoryRepository.findOne(categoryId);
        if (category == null) {
            return Helper.createResponse(Helper.createMessage("category not found"), HttpStatus.BAD_REQUEST);
        }
        User user = userRepository.findOne(Helper.getAuthenticatedUserId());

        Integer total = -1;
        Integer successCount = 0;
        List<String[]> failed = new ArrayList<>();

        if (file == null || file.isEmpty()) {
            return Helper.createResponse(Helper.createMessage("You did not upload file"), HttpStatus.BAD_REQUEST);
        }

        if (file != null && !file.isEmpty()) {

            BufferedReader br = null;
            String line = "";
            String splitBy = ",";

            try {
                ByteArrayInputStream b = new ByteArrayInputStream(file.getBytes());
                br = new BufferedReader(new InputStreamReader(b));

                boolean headingRow = true;

                while ((line = br.readLine()) != null) {

                    total += 1;

                    if (headingRow) {
                        headingRow = false;
                        continue;
                    }
                    // split on comma(',')
                    String[] data = line.split(splitBy);

                    System.out.println(data[0] + "," + data[1] + "," + data[2] + "," + data[3] + "," + data[4]);

                    Quiz quiz = new Quiz();
                    quiz.setLevel(level);
                    quiz.setSubject(subject);
                    quiz.setDifficulty(difficulty);
                    quiz.setCategory(category);
                    quiz.setQuestion(Helper.replaceNonPrintableUnicodeCharacters(data[0]));
                    quiz.setOptionOne(Helper.replaceNonPrintableUnicodeCharacters(data[1]));
                    quiz.setOptionTwo(Helper.replaceNonPrintableUnicodeCharacters(data[2]));
                    quiz.setOptionThree(Helper.replaceNonPrintableUnicodeCharacters(data[3]));
                    quiz.setOptionFour(Helper.replaceNonPrintableUnicodeCharacters(data[4]));
                    quiz.setUser(user);

                    if (quizRepository.save(quiz) != null) {
                        successCount++;
                        System.out.println("SUCCESS");
                    } else {
                        failed.add(data);
                        System.out.println("IMESHINDIKANA KUSAJILI");
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
                return Helper.createResponse(null, HttpStatus.BAD_REQUEST);
            } finally {

                if (br != null) {
                    try {
                        br.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        String message = successCount + "/" + total;

        return Helper.createResponse(message, HttpStatus.OK);
    }
}
