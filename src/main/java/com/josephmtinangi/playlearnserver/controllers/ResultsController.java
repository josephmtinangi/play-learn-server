package com.josephmtinangi.playlearnserver.controllers;

import com.josephmtinangi.playlearnserver.models.Battle;
import com.josephmtinangi.playlearnserver.models.Result;
import com.josephmtinangi.playlearnserver.repositories.BattleRepository;
import com.josephmtinangi.playlearnserver.repositories.ResultRepository;
import com.josephmtinangi.playlearnserver.repositories.UserRepository;
import com.josephmtinangi.playlearnserver.utilities.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/results")
public class ResultsController {

    @Autowired
    private BattleRepository battleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ResultRepository resultRepository;

    @RequestMapping(path = "/battles/{battleId}/score/{score}", method = RequestMethod.POST)
    public ResponseEntity<?> store(@PathVariable Long battleId, @PathVariable Integer score) {

        Battle battle = battleRepository.findOne(battleId);

        Result result = new Result();
        result.setBattle(battle);
        result.setUser(userRepository.findOne(Helper.getAuthenticatedUserId()));
        result.setScore(score);

        Result newResult = resultRepository.save(result);

        return Helper.createResponse(newResult, HttpStatus.CREATED);
    }
}
