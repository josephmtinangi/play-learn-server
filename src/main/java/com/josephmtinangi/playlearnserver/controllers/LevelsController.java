package com.josephmtinangi.playlearnserver.controllers;

import com.josephmtinangi.playlearnserver.models.Level;
import com.josephmtinangi.playlearnserver.repositories.LevelRepository;
import com.josephmtinangi.playlearnserver.utilities.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/levels")
public class LevelsController {

    @Autowired
    private LevelRepository levelRepository;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<?> index() {

        return Helper.createResponse(levelRepository.findAll(), HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> show(@PathVariable Long id) {

        Level level = levelRepository.findOne(id);

        if (level == null) {
            return Helper.createResponse(Helper.createMessage("Level not found"), HttpStatus.NOT_FOUND);
        }

        return Helper.createResponse(level, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<?> store(@ModelAttribute Level level) {

        return Helper.createResponse(levelRepository.save(level), HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.PATCH)
    public ResponseEntity<?> update(@PathVariable Long id, @ModelAttribute Level level) {

        Level oldLevel = levelRepository.findOne(id);

        if (oldLevel == null) {
            return Helper.createResponse(Helper.createMessage("Level not found"), HttpStatus.NOT_FOUND);
        }
        level.setId(oldLevel.getId());
        levelRepository.save(level);

        return Helper.createResponse(level, HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> destroy(@PathVariable Long id) {

        Level level = levelRepository.findOne(id);

        if (level == null) {
            return Helper.createResponse(Helper.createMessage("Level not found"), HttpStatus.NOT_FOUND);
        }
        levelRepository.delete(level);

        return Helper.createResponse(null, HttpStatus.OK);
    }
}
