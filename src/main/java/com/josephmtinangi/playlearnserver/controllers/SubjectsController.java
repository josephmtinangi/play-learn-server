package com.josephmtinangi.playlearnserver.controllers;

import com.josephmtinangi.playlearnserver.models.Subject;
import com.josephmtinangi.playlearnserver.repositories.SubjectRepository;
import com.josephmtinangi.playlearnserver.utilities.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/subjects")
public class SubjectsController {

    @Autowired
    private SubjectRepository subjectRepository;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<?> index() {

        return Helper.createResponse(subjectRepository.findAll(), HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<?> store(@ModelAttribute Subject subject) {

        return Helper.createResponse(subjectRepository.save(subject), HttpStatus.OK);
    }
}
