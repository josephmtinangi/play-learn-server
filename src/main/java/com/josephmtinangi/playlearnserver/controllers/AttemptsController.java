package com.josephmtinangi.playlearnserver.controllers;

import com.josephmtinangi.playlearnserver.models.Attempt;
import com.josephmtinangi.playlearnserver.models.Battle;
import com.josephmtinangi.playlearnserver.models.Quiz;
import com.josephmtinangi.playlearnserver.models.User;
import com.josephmtinangi.playlearnserver.repositories.AttemptRepository;
import com.josephmtinangi.playlearnserver.repositories.BattleRepository;
import com.josephmtinangi.playlearnserver.repositories.QuizRepository;
import com.josephmtinangi.playlearnserver.repositories.UserRepository;
import com.josephmtinangi.playlearnserver.utilities.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/attempts")
public class AttemptsController {

    @Autowired
    private BattleRepository battleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private QuizRepository quizRepository;

    @Autowired
    private AttemptRepository attemptRepository;

    @RequestMapping(path = "/battles/{battleId}/users/{userId}", method = RequestMethod.GET)
    public ResponseEntity<?> summary(@PathVariable Long battleId, @PathVariable Long userId){

        Battle battle = battleRepository.findOne(battleId);
        if(battle==null){
            return Helper.createResponse(Helper.createMessage("Battle not found"),HttpStatus.NOT_FOUND);
        }
        User user = userRepository.findOne(userId);
        if(user==null){
            return Helper.createResponse(Helper.createMessage("User not found"),HttpStatus.NOT_FOUND);
        }

        return Helper.createResponse(attemptRepository.findAllByBattleIdAndUserId(battle.getId(), user.getId()), HttpStatus.OK);
    }

    @RequestMapping(path = "/battles/{battleId}/quizzes/{quizId}/response/{response}/answer/{answer}/result/{result}", method = RequestMethod.POST)
    public ResponseEntity<?> store(@PathVariable Long battleId, @PathVariable Long quizId, @PathVariable String response, @PathVariable String answer, @PathVariable String result) {

        Battle battle = battleRepository.findOne(battleId);
        if (battle == null) {
            return Helper.createResponse(Helper.createMessage("Battle not found"), HttpStatus.BAD_REQUEST);
        }
        Quiz quiz = quizRepository.findOne(quizId);
        if (quiz == null) {
            return Helper.createResponse(Helper.createMessage("Quiz not found"), HttpStatus.BAD_REQUEST);
        }
        User user = userRepository.findOne(Helper.getAuthenticatedUserId());

        Attempt attempt = new Attempt();
        attempt.setBattle(battle);
        attempt.setUser(user);
        attempt.setQuiz(quiz);
        attempt.setResponse(response);
        attempt.setAnswer(answer);
        attempt.setResult(result);

        Attempt newAttempt = attemptRepository.save(attempt);

        return Helper.createResponse(newAttempt, HttpStatus.CREATED);
    }
}
