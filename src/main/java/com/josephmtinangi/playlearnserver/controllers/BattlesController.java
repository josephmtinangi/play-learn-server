package com.josephmtinangi.playlearnserver.controllers;

import com.josephmtinangi.playlearnserver.models.*;
import com.josephmtinangi.playlearnserver.repositories.*;
import com.josephmtinangi.playlearnserver.services.QuizService;
import com.josephmtinangi.playlearnserver.utilities.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(path = "/battles")
public class BattlesController {

    @Autowired
    private BattleRepository battleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private LevelRepository levelRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Autowired
    private DifficultyRepository difficultyRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private BattleQuizRepository battleQuizRepository;

    @Autowired
    private QuizService quizService;

    @Autowired
    private AchievementRepository achievementRepository;

    @RequestMapping(path = "", method = RequestMethod.GET)
    public ResponseEntity<?> index(Pageable pageable) {

        return Helper.createResponse(battleRepository.findAll(pageable), HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> show(@PathVariable Long id) {

        Battle battle = battleRepository.findOne(id);

        if (battle == null) {
            return Helper.createResponse(null, HttpStatus.BAD_REQUEST);
        }

        return Helper.createResponse(battle, HttpStatus.OK);
    }

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<?> store(@ModelAttribute Battle battle, @RequestParam int questions) {

        Level level;
        if (battle.getLevel().getId() == 0) {
            level = levelRepository.findOne(Helper.getRandomNumber(levelRepository.count()));
        } else {
            level = levelRepository.findOne(battle.getLevel().getId());
        }
        Subject subject;
        if (battle.getSubject().getId() == 0) {
            subject = subjectRepository.findOne(Helper.getRandomNumber(subjectRepository.count()));
        } else {
            subject = subjectRepository.findOne(battle.getSubject().getId());
        }
        Difficulty difficulty;
        if (battle.getDifficulty().getId() == 0) {
            difficulty = difficultyRepository.findOne(Helper.getRandomNumber(difficultyRepository.count()));
        } else {
            difficulty = difficultyRepository.findOne(battle.getDifficulty().getId());
        }
        Category category;
        if (battle.getCategory().getId() == 0) {
            category = categoryRepository.findOne(Helper.getRandomNumber(categoryRepository.count()));
        } else {
            category = categoryRepository.findOne(battle.getCategory().getId());
        }

        List<Quiz> quizzes = quizService.findRandomQuizzes(level.getId(), subject.getId(), difficulty.getId(), category.getId(), questions);

        Battle newBattle = null;
        if (quizzes.size() > 0) {
            battle.setUser(userRepository.findOne(Helper.getAuthenticatedUserId()));
            battle.setName(UUID.randomUUID().toString());
            newBattle = battleRepository.save(battle);

            ArrayList<BattleQuiz> battleQuizArrayList = new ArrayList<>();
            for (Quiz quiz : quizzes) {
                BattleQuiz battleQuiz = new BattleQuiz();
                battleQuiz.setBattle(newBattle);
                battleQuiz.setQuiz(quiz);
                battleQuizArrayList.add(battleQuiz);
            }
            battleQuizRepository.save(battleQuizArrayList);
        } else {
            return Helper.createResponse(quizzes.size(), HttpStatus.OK);
        }

        return Helper.createResponse(newBattle.getId(), HttpStatus.CREATED);
    }

    @RequestMapping(path = "/{id}/quizzes", method = RequestMethod.GET)
    public ResponseEntity<?> quizzes(@PathVariable Long id) {

        Battle battle = battleRepository.findOne(id);
        if (battle == null) {
            return Helper.createResponse(null, HttpStatus.BAD_REQUEST);
        }

        return Helper.createResponse(battle.getBattleQuizes(), HttpStatus.OK);
    }

    @RequestMapping(path = "/{id}/participants", method = RequestMethod.GET)
    public ResponseEntity<?> participants(@PathVariable Long id) {

        Battle battle = battleRepository.findOne(id);
        if (battle == null) {
            return Helper.createResponse(Helper.createMessage("Battle not found"), HttpStatus.BAD_REQUEST);
        }

        List<Achievement> achievements = achievementRepository.findAllByBattleId(battle.getId());

        List<HashMap<String, Object>> participants = new ArrayList<>();
        for (Achievement achievement : achievements) {
            HashMap<String, Object> participantMap = new HashMap<>();
            participantMap.put("user", achievement.getUser());
            participantMap.put("points", achievement.getPoints());
            participantMap.put("quizzesCount", achievement.getBattle().getBattleQuizes().size());
            participants.add(participantMap);
        }

        return Helper.createResponse(participants, HttpStatus.OK);
    }
}
