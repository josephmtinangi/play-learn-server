package com.josephmtinangi.playlearnserver.controllers;

import com.josephmtinangi.playlearnserver.models.UserAuthentication;
import com.josephmtinangi.playlearnserver.repositories.UserAuthenticationRepository;
import com.josephmtinangi.playlearnserver.repositories.UserRepository;
import com.josephmtinangi.playlearnserver.utilities.Helper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path = "/user-authentications")
public class UserAuthenticationsController {

    @Autowired
    private UserAuthenticationRepository userAuthenticationRepository;

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(path = "", method = RequestMethod.POST)
    public ResponseEntity<?> store(HttpServletRequest request) {

        UserAuthentication userAuthentication = new UserAuthentication();
        userAuthentication.setUser(userRepository.findOne(Helper.getAuthenticatedUserId()));


        UserAuthentication newUserAuthentication = userAuthenticationRepository.save(userAuthentication);

        return Helper.createResponse(newUserAuthentication, HttpStatus.CREATED);
    }
}
