package com.josephmtinangi.playlearnserver.controllers;

import com.josephmtinangi.playlearnserver.utilities.Helper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.File;
import java.io.IOException;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
@RequestMapping(path = "/attachments")
public class AttachmentController {

    @Value("${ROOT_STORAGE}")
    private String rootStorage;

    @RequestMapping(path = "/{location}/{filename:.+}", method = RequestMethod.GET)
    public ResponseEntity<?> load(@PathVariable String location, @PathVariable String filename) throws IOException {

        String filePath = rootStorage + "/" + location + "/" + filename;
        File file = new File(filePath);
        Path path = Paths.get(file.getAbsolutePath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
        String mimeType = URLConnection.guessContentTypeFromName(file.getName());
        if (mimeType == null) {
            mimeType = "application/octet-stream";
        }

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set("Content-Disposition",
                "attachment; filename=" + filename + "." + Helper.getFileExtension(filename));

        return ResponseEntity.ok().contentLength(file.length()).contentType(MediaType.parseMediaType(mimeType))
                .headers(httpHeaders).body(resource);
    }
}
