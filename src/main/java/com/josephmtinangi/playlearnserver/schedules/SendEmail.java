package com.josephmtinangi.playlearnserver.schedules;

import com.josephmtinangi.playlearnserver.models.EmailOut;
import com.josephmtinangi.playlearnserver.repositories.EmailOutRepository;
import com.josephmtinangi.playlearnserver.services.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SendEmail {

	private static final Logger log = LoggerFactory.getLogger(SendEmail.class);

	@Autowired
	private EmailService emailService;

	@Autowired
	private EmailOutRepository emailOutRepository;

	@Value("${EMAIL_FROM_ADDRESS}")
	private String fromEmail;

	@Scheduled(fixedRate = 5000)
	public void invoke() {

		List<EmailOut> unsentEmails = emailOutRepository.findUnsentEmails();

		if (unsentEmails.size() > 0) {

			log.info("Found " + unsentEmails.size() + " unsent emails");

			for (EmailOut email : unsentEmails) {
				try {
					emailService.sendHtmlEmail(email, fromEmail);
					// mark as sent
					email.setSentStatus(new Boolean(true));
					// mark as succeeded
					email.setSucceeded(new Boolean(true));
				} catch (Exception e) {
					e.printStackTrace();
					email.setSentStatus(new Boolean(true));
				}
				emailOutRepository.save(email);
			}

			log.info("Done sending them all");
		}
	}
}
