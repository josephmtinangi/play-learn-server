package com.josephmtinangi.playlearnserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class PlayLearnServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(PlayLearnServerApplication.class, args);
	}
}
