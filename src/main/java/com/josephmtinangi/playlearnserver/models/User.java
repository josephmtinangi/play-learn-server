package com.josephmtinangi.playlearnserver.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.josephmtinangi.playlearnserver.utilities.Helper;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;

    private String middleName;

    private String lastName;

    @Column(columnDefinition = "text")
    private String bio;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(unique = true)
    private String phone;

    @Column(unique = true, nullable = false)
    private String email;

    @Column(nullable = false)
    private String password;

    private String avatarUrl;

    private Date verifiedAt;

    private String token;

    private Long points;

    @JsonIgnore
    @OneToMany(mappedBy = "user", targetEntity = Battle.class, fetch = FetchType.LAZY)
    private List<Battle> battles;

    @JsonIgnore
    @OneToMany(mappedBy = "user", targetEntity = Participant.class, fetch = FetchType.LAZY)
    private List<Battle> participatedBattles;

    @JsonIgnore
    @OneToMany(mappedBy = "user", targetEntity = Achievement.class, fetch = FetchType.LAZY)
    private List<Achievement> achievements;

    @Column(name = "created_at", updatable = false)
    private Date createdAt;

    @Column(name = "updated_at")
    private Date updatedAt;

    @PrePersist
    void createdAt() {
        this.createdAt = this.updatedAt = new Date();
    }

    @PreUpdate
    void updatedAt() {
        this.updatedAt = new Date();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public Date getVerifiedAt() {
        return verifiedAt;
    }

    public void setVerifiedAt(Date verifiedAt) {
        this.verifiedAt = verifiedAt;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public List<Battle> getBattles() {
        return battles;
    }

    public void setBattles(List<Battle> battles) {
        this.battles = battles;
    }

    public List<Battle> getParticipatedBattles() {
        return participatedBattles;
    }

    public void setParticipatedBattles(List<Battle> participatedBattles) {
        this.participatedBattles = participatedBattles;
    }

    public List<Achievement> getAchievements() {
        return achievements;
    }

    public void setAchievements(List<Achievement> achievements) {
        this.achievements = achievements;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getAvatar() {

        if (this.avatarUrl == null || this.avatarUrl.isEmpty()) {
            return null;
        }

        return Helper.getAttachmentUrl() + "/" + this.avatarUrl;
    }

    public Integer getCreatedBattlesCount() {
        return this.battles.size();
    }

    public Integer getParticipatedBattlesCount() {
        return this.participatedBattles.size();
    }

    public Long getPoints() {
        return points;
    }

    public void setPoints(Long points) {
        this.points = points;
    }
}
