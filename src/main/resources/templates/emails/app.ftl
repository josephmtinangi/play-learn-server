<!DOCTYPE html>
<html>
<head>
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons' rel="stylesheet">
    <link href="https://unpkg.com/vuetify/dist/vuetify.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">

    <title>${subject}</title>
</head>
<body>
<div id="app">
    <v-app>
        <v-content>
            <v-container>
                <v-layout>
                    <v-flex sm6 offset-sm3>
                        <div class="while elevation-2">
                            <v-toolbar flat dense class="cyan" dark>
                                <v-toolbar-title>
                                    Hello ${name}
                                </v-toolbar-title>
                            </v-toolbar>
                            <div class="pl-4 pr-4 pt-2 pb-2">
                                <slot>
                                ${content}
                                    <br><br>
                                    Regards, <br>
                                    Support Team <br>
                                </slot>
                            </div>
                        </div>
                    </v-flex>
                </v-layout>
            </v-container>
        </v-content>
    </v-app>
</div>

<script src="https://unpkg.com/vue/dist/vue.js"></script>
<script src="https://unpkg.com/vuetify/dist/vuetify.js"></script>
<script>
    new Vue({el: '#app'})
</script>
</body>
</html>